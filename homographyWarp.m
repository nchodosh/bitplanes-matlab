function [ wpts ] = homographyWarp(pts, p, cpts)
    H = paramsToHomography(p, cpts);
    wpts = [ pts, ones(size(pts, 1), 1) ] * H';
    wpts = wpts(:, 1:2)  ./ repmat(wpts(:, 3), 1, 2);
end
