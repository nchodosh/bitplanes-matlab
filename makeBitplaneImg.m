function [ img ] = makeBitplaneImg(planes)
    [ nr, nc, ~ ] = size(planes);
    img = zeros(8*(nr+1), nc, 3);
    divider = repmat(reshape([ 0, 176, 182 ], 1, 1, 3), 1, nc);
    for b = 1:8
        plane = 255*planes(:, :, b);
        section = ((b-1)*(nr+1) + 1):(b*(nr+1));
        img(section, :, :) = [ repmat(plane, 1, 1, 3); divider ];
    end
end
