function params = homographyToParams(H, cpts)
    pts = H * [ cpts, ones(4, 1) ]';
    pts = pts(1:2, :) ./ repmat(pts(3, :), 2, 1) - cpts';
    params = pts(:);
end
