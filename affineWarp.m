function [ wpts ] = affineWarp(pts, p)
    T = [ 1 + p(1), p(2), p(5);
          p(3), 1 + p(4), p(6) ];
    wpts = (T * [ pts, ones(size(pts, 1), 1) ]')';
end
