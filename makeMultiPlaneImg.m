function [ J ] = makeMultiPlaneImg(I)
 [ d, nr, nc ] = size(I);
 J = zeros(nr*d, nc);
 for i = 1:d
     J(nr*(i-1)+1:nr*i, :) = reshape(I(i, :, :), nr, nc);
 end
end