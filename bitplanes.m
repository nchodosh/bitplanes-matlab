I1 = imresize(rgb2gray(imread('frame0.png')), 0.20);
I2 = imresize(rgb2gray(imread('frame1.png')), 0.20);

I1 = imgaussfilt(I1, 2);
I2 = imgaussfilt(I2, 2);

[nr, nc] = size(I1);

num_pts = 25; % number of points to track
rad = 5; % window radius
points = detectMinEigenFeatures(I1((rad+1):(nr-rad), (rad+1):(nc-rad))); %% Should I use the SIFT detector here?
pts = round(points.selectStrongest(num_pts).Location) + ...
      repmat([ rad, rad ], [ num_pts, 1]);

% phi{1,2} holds the bitplane for each pixel
phi1 = zeros([nr, nc, 8]);  
phi2 = zeros([nr, nc, 8]);
%holds the derivatives of the bitplanes
dphi1dx = zeros([nr, nc, 8]);
dphi1dy = zeros([nr, nc, 8]);

[x, y] = meshgrid(1:nc, 1:nr);
inds_0 = sub2ind([nr, nc], y(:), x(:));

cnt = 1;





iterations = 15;
ps = zeros(num_pts, 2);
[ win_x, win_y ] = meshgrid(-rad:rad, -rad:rad);

rep_idx = repmat(1:numel(win_x(:)), 8, 1);
rep_idx = rep_idx(:);

x_pts = win_x(rep_idx(:));
y_pts = win_y(rep_idx(:));       
b_pts = repmat(1:8, numel(win_x(:)), 1);
for n = 1:num_pts
    p = [ 0; 0 ];
    jac_inds = sub2ind([nr, nc, 8], y_pts(:) + pts(n, 2), x_pts(:) + pts(n, 1), b_pts(:));    
    J = [ dphi1dx(jac_inds), dphi1dy(jac_inds) ];
    H = J' * J;
    
    for m = 1:iterations
        planes = zeros(nr, nc, 8);
        for i = 1:8 %%very slow, I should only warp the relavent part of the image
            plane2 = phi2(:, :, i);
            plane2 = imtranslate(plane2, -p');
            planes(:, :, i) = plane2;
        end
        pt_num = 1;

        residual_inds = sub2ind([nr, nc, 8], y_pts(:) + pts(n, 2), x_pts(:) + ...
                                pts(n, 1), b_pts(:));

        e = planes(residual_inds) - phi1(residual_inds);        
        %fprintf('%d %f\n', m, norm(e));
        dp = H \ ( J' * e ); %% H is only 2x2 
        p = p - dp;
    end
    ps(n, :) = p';
end