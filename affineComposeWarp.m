function [ new_p ] = affineComposeWarp(dp, p)
    A = [ 1 + dp(1), dp(2);
          dp(3), 1 + dp(4) ];
    b = [ dp(5); dp(6) ];
    T = [ 1 + p(1), p(2), p(5);
          p(3), 1 + p(4), p(6);
          0, 0, 1 ];
    dTinv = [ inv(A), - A \ b ;
              0, 0, 1];
    new_T = dTinv * T;
    new_T = new_T / new_T(3, 3);
    new_p = [ new_T(1, 1) - 1; new_T(1, 2); new_T(2, 1); new_T(2, 2) - 1; ...
              new_T(1, 3); new_T(2, 3) ];
end
