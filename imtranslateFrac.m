function [ J ] = imtranslateFrac(I, t, shape)
    nr = size(I, 1);
    nc = size(I, 2);
    d = size(I, 3);
    if strcmp(shape, 'same')
        I = [ zeros(1, nc + 2, d);
              zeros(nr, 1, d), I, zeros(nr, 1, d);
              zeros(1, nc + 2, d) ];
    elseif not(strcmp(shape, 'valid'))
        disp('ERROR: shape can only be <same> or <valid>');
    end
    nr = size(I, 1);
    nc = size(I, 2);

    if (t(1) < 0) 
        dx = -1; 
    else 
        dx = 1; 
    end
    if (t(2) < 0) 
        dy = -1; 
    else 
        dy = 1; 
    end 
    
    x_shift = (1:nc) - dx;
    x_shift(x_shift > nc) = nc;
    x_shift(x_shift < 1) = 1;    
    
    y_shift = (1:nr) - dy;
    y_shift(y_shift > nr) = nr;
    y_shift(y_shift < 1) = 1;    
    
    I_x = abs(t(1)) * I(1:nr, x_shift, :) + ...
          (1 - abs(t(1))) * I(:, :, :);
    J = abs(t(2)) * I_x(y_shift, 1:nc, :) + ...
        (1 - abs(t(2))) * I_x(:, :, :);
    J = J(2:(nr-1), 2:(nc-1), :);
end
