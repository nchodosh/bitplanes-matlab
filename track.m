function [ new_pts, params, iters, imgs ] = track(I1, I2, pts, weights, params_init, get_debug)

    rad = 10; % window radius
    diam = 2*rad + 1;
    num_pts = size(pts, 1);
    [nr, nc] = size(I1);

    % phi{1,2} holds the bitplane for each pixel
    phi1 = compute_channels(I1);
    phi2 = compute_channels(I2);
    
    phi1_perm = permute(phi1, [ 3 1 2 ] );
    phi2_perm = permute(phi2, [ 3 1 2 ] );
    
    %holds the derivatives of the bitplanes
    [ dpdx, dpdy ] = channel_derivs(phi1_perm);


    iterations = 50;
    params = zeros(num_pts, 2);

    imgs = cell(num_pts, 1);
    iters = zeros(num_pts, 1);
    for n = 1:num_pts
        if (pts(n, 2) <= nr - rad && pts(n, 2) >= 1 + rad && ...
            pts(n, 1) <= nc - rad && pts(n, 1) >= 1 + rad)
            p = params_init(n, :)';
            dpdx_roi = dpdx(:, (-rad:rad) + pts(n, 2), (-rad:rad) + pts(n, 1));
            dpdy_roi = dpdy(:, (-rad:rad) + pts(n, 2), (-rad:rad) + pts(n, 1));
            J = [ dpdx_roi(:), dpdy_roi(:) ];
            H =  J' * J;

            imgs_cur = zeros(0, 0, 0, iterations);
            if cond(H) < 1000 % Make sure H isn't close to singular
                for m = 1:iterations
                    if (pts(n, 2) + round(p(2)) <= rad+1 || ...
                        pts(n, 2) + round(p(2)) >= nr-(rad+1) || ...
                        pts(n, 1) + round(p(1)) <= rad+1 || ...
                        pts(n, 1) + round(p(1)) >= nc-(rad+1))
                        break;
                    end
                    planes_roi = phi2_perm(:, (-(rad+1):(rad+1)) + pts(n, 2) + round(p(2)), ...
                                           (-(rad+1):(rad+1)) + pts(n, 1) + round(p(1)));
                    %planes = imtranslate(planes_roi, -(p - round(p))'); 
                    
                    warped_planes = imtranslateFrac(permute(planes_roi, [2 3 1]),...
                                                    -(p - round(p))', ...
                                                    'valid');
                    warped_planes = permute(warped_planes, [ 3 1 2 ] );
                    template_planes = phi1_perm(:, (-rad:rad) + pts(n, 2), ...
                                                (-rad:rad) + pts(n, 1));
                    
                    %%%MAKING DEBUGGING IMAGES
                    if get_debug(n)
                        i2_roi = I2((-(rad+1):(rad+1)) + pts(n, 2) + round(p(2)), ...
                                    (-(rad+1):(rad+1)) + pts(n, 1) + round(p(1)));
                        i2_warped = imtranslateFrac(i2_roi, - (p - round(p))', ...
                                                    'valid');
                        i1_roi = I1((-rad:rad) + pts(n, 2), ...
                                    (-rad:rad) + pts(n, 1));
                        wplanes_img = makeBitplaneImg(permute(warped_planes, ...
                                                              [ 2 3 1]));
                        tplanes_img = makeBitplaneImg(permute(template_planes, ...
                                                              [ 2 3 1 ]));
                        cost_img = makeCostImg(phi1, phi2, pts(n, :), p, ...
                                               rad);

                        img_cur = [ repmat(i2_warped, 1, 1, 3), ...
                                    repmat(i1_roi, 1, 1, 3); ...
                                    cost_img, zeros(diam, diam, 3);
                                    wplanes_img, tplanes_img];
                        divider = reshape([0 176 182], 1, 1, 3);
                        img_cur = [ img_cur(:, 1:diam, :), ...
                                    repmat(divider, size(img_cur, 1), 1), ...
                                    img_cur(:, diam+1:end, :) ];
                        if m == 1
                            imgs_cur = zeros([ size(img_cur), iterations ]);
                        end
                        imgs_cur(:, :, :, m) = img_cur;
                    end
                    %%%DONE WITH DEBUGGING IMAGES
                    
                    e = warped_planes - template_planes;
                    e = e(:);
                    
                    dp = H \ (J' * e); %% H is only 2x2 
                    p = p - dp;
                    if (norm(dp) < 0.05)
                        break;
                    end
                end
                if get_debug(n)
                    imgs_cur = imgs_cur(:, :, :, 1:m);
                    imgs{n} = imgs_cur;
                end
                iters(n) = m;
                params(n, :) = p';
            end
        end
    end
    new_pts = pts + params;                
end