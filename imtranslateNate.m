function [ J ] = imtranslateNate(I, t)
    I = padarray(I, [ 1 1 ]);
    t_integer = fix(t);
    t_frac = t - t_integer;
    
    nr = size(I, 1);
    nc = size(I, 2);
    
    x_shift = (1:nc) - t_integer(1);
    x_shift(x_shift > nc) = nc;
    x_shift(x_shift < 1) = 1;    
    
    y_shift = (1:nr) - t_integer(2);
    y_shift(y_shift > nr) = nr;
    y_shift(y_shift < 1) = 1;    
    
    J_int = I(y_shift, x_shift, :);
    
    J = imtranslateFrac(J_int, t_frac, 'valid');
end