function [ new_params ] = translationIncreaseParams(p)
    new_params = 2 * p;
end
