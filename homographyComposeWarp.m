function [ new_p ] = homographyComposeWarp(dp, p, cpts)
    Hp = paramsToHomography(p, cpts);
    Hdp = paramsToHomography(dp, cpts);    
    Hnew = Hdp \ Hp;
    Hnew = Hnew / Hnew(3, 3);
    new_p = homographyToParams(Hnew, cpts);
end
