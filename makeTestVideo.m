function [] = makeTestVideo()
    nr = 480;
    nc = 640;

    background1 = 64/255;
    background2 = 192/255;
    background = [ background1*ones(480, 320), background2*ones(480, 320) ];
    % shapeInserter = vision.ShapeInserter('Shape','Lines','BorderColor','Custom',...
    %                                      'CustomBorderColor',bar_color,...
    %                                      'FillColor', 'Custom',...
    %                                      'CustomFillColor', bar_color);

    vw = VideoWriter('background_change');
    open(vw);

    frame = drawBar(background, 320 + 90, 240);
    prev_frame = frame;
    for t = 1:100
        noise = 5.0/255.0*repmat(rand(nr, nc), 1, 1, 3);
        final_frame = imgaussfilt(0.9 * frame + 0.1 * prev_frame + noise, 1.2);
        writeVideo(vw, final_frame);
        x = 320 + 90*cos((2*pi*t/159) ^ 1.5);
        prev_frame = frame;
        frame = drawBar(background, x, 240);
    end
    close(vw);
end


function [ J ]  = drawBar(background, x, y)
    bar_color = 128/255;
    bar_size = [ 133, 200 ];
    J = insertShape(background, 'FilledRectangle', ...
                    [ ([ x, y ] - bar_size/2), bar_size ],...
                    'Color', [bar_color, bar_color, bar_color],...
                    'Opacity', 1);
    J = permute(insertText(permute(J, [ 1 2 3]), [ x, y-40; x, y; x, y+40 ],...
                           {'THIS IS', 'NOT', 'A BOX'}, ...
                           'TextColor', 1.3*[bar_color, bar_color, bar_color],...
                           'AnchorPoint', 'Center', ...
                           'BoxOpacity', 0,...
                           'FontSize', 32,...
                           'Font', 'Arial Bold'), [1 2 3]);
end

