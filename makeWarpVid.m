function [ output_frames ] = makeWarpVid(frame_nums, frames, params, mask, scale, cpts)
    output_frames = cell(0);
    cnt = 1;
    for i = frame_nums
        output_frame = imgaussfilt(double(imresize(rgb2gray(frames{i}), scale)), ...
                                   2);
        output_frames{cnt} = warpImage(output_frame, params{i}, ...
                                       @(pts, p)homographyWarp(pts, p, cpts));
        cnt = cnt + 1;
    end
end