num_pts = 30;
num_levels = 3;
scale = 1;
g = normpdf(-10:10, 0, 10)';
%weights = repmat(g*g', 1, 1, 8);
weights = ones(21, 21, 8);
v = VideoReader('background_change.avi');

frame1_full = readFrame(v);
frame1 = imresize(frame1_full, scale);
frame1_gray = double(imgaussfilt(rgb2gray(frame1), 2));

[nr_full, nc_full, ~] = size(frame1_full);
[nr, nc] = size(frame1_gray);

%sample_width = 5;
%figure('Name', 'Select Hand'); imagesc(frame1); bbox  = getrect; close;
%bbox = round(bbox);
% [x, y] = meshgrid(bbox(1):sample_width:(bbox(1) + bbox(3)), ...
%                  bbox(2):sample_width:(bbox(2) + bbox(4)));
% pts = [ x(:), y(:) ];
% num_pts = size(pts, 1);
%points = detectMinEigenFeatures(frame1_gray, 'ROI', bbox);
%pts = round(points.selectStrongest(num_pts).Location);

[ M, xi, yi ] = roipoly(frame1);
pts = getPointsInPoly(frame1_gray, ...
                      M, [ min(ceil(xi)), min(ceil(yi)), max(floor(xi)), max(floor(yi)) ], ...
                      num_pts);
num_pts = size(pts, 1);
new_pts = pts;

red = uint8([255 0 0]);
shapeInserter = vision.ShapeInserter('Shape','Circles','BorderColor','Custom',...
                                     'CustomBorderColor',red);
vw = VideoWriter('output');
open(vw);

frame_num = 1;

params = zeros(num_pts, 2);

imgs = cell(0);
output_video = zeros(nr_full, nc_full, 3, 0);
traj = cell(0);
cnt = 1;
get_debug = false(num_pts, 1);
get_debug([1, 4]) = true;
while hasFrame(v) && frame_num < 400
    
    fprintf('Processing frame %d\n', frame_num);
    frame_num = frame_num + 1;
    
    output_frame = step(shapeInserter, frame1_full, ...
                        int32([ new_pts/scale, 3*ones(num_pts, 1) ]));
    writeVideo(vw, output_frame);


    output_video(:, :, :, cnt) = output_frame;
    traj{cnt} = new_pts/scale;

    
    frame2_full = readFrame(v);
    frame2 = imresize(frame2_full, scale);
    frame2_gray = double(imgaussfilt(rgb2gray(frame2), 2));
    
    [ new_pts, new_params, ~, new_imgs ] = trackPyramid(frame1_gray, frame2_gray, pts, ...
                                              num_levels, weights, params, get_debug);
    imgs{cnt} = new_imgs;
    cnt = cnt + 1;
    
    frame1_full = frame2_full;
    %frame1 = frame2;
    %frame1_gray = frame2_gray;
    %pts = new_pts;
    params = new_params;
end

close(vw);