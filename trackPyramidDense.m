function [ new_pts, params, iters, residuals, imgs ] = trackPyramidDense(I1, I2, pts, levels, ...
                                                      track, params_init, ...
                                                      increase_params, get_debug, ...
                                                      iterations, min_disp)

    pyr1 = makePyramid(I1, levels);
    pyr2 = makePyramid(I2, levels);
    ptPyr = makePtPyramid(pts, levels);
    params = params_init;
    iters = 0;
    imgs = zeros(0);
    for i = 1:levels
        [ new_pts, cur_params, cur_iters, residuals, cur_imgs ] = track(pyr1{i}, pyr2{i}, ...
                                                          ptPyr{i}, params, ...
                                                          get_debug, iterations, ...
                                                          min_disp);
        params = increase_params(cur_params);
        iters = iters + cur_iters;
        imgs{i} = cur_imgs;
        fprintf('Finished level %d\n\n', i);
    end
    params = cur_params;
end
