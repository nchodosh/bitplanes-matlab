function [ phi ] = compute_channels(I)
    [nr, nc] = size(I);
    phi = zeros(nr, nc, 8);
    [x, y] = meshgrid(1:nc, 1:nr);
    inds_0 = sub2ind([nr, nc], y(:), x(:));
    cnt = 1;
    %% Compute channels
    for dx = -1:1
        for dy = -1:1
            if dx ~= 0 || dy ~= 0
                xp = x + dx;
                yp = y + dy;
                xp(xp > nc) = nc;
                yp(yp > nr) = nr;
                xp(xp < 1) = 1;
                yp(yp < 1) = 1;
                inds = sub2ind([nr, nc], yp(:), xp(:));
                phi(:, :, cnt) = reshape(I(inds_0) > I(inds), nr, nc);
                cnt = cnt + 1;
            end
        end
    end
end