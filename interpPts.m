function [ J ] = interpPts(I, pts)

    pts = pts + 1;
    [ d, nr, nc ] = size(I);
    Ipad = zeros(d, nr + 2, nc + 2);
    for i = 1:d
        Ipad(i, :, :) = padarray(reshape(I(i, :, :), nr, nc), [1 1]);
    end
    nr = nr + 2; nc = nc + 2;
    pts(pts(:, 1) > nc, 1) = nc;
    pts(pts(:, 2) > nr, 2) = nr;    
    pts(pts(:, 1) < 1, 1) = 1;
    pts(pts(:, 2) < 1, 2) = 1;
    ipts = floor(pts);
    inds = sub2ind([nr, nc], ipts(:, 2), ipts(:, 1));
    xplusone = ipts(:, 1) + 1;
    xplusone(xplusone > nc) = nc;
    yplusone = ipts(:, 2) + 1;
    yplusone(yplusone > nr) = nr;
    inds_xshift = sub2ind([nr, nc], ipts(:, 2), xplusone);
    inds_yshift = sub2ind([nr, nc], yplusone, ipts(:, 1));
    inds_xyshift = sub2ind([nr, nc], yplusone, xplusone);
    x_weights = repmat(1 - (pts(:, 1) - ipts(:, 1))', d, 1);
    y_weights = repmat(1 - (pts(:, 2) - ipts(:, 2))', d, 1);    
    
    J_xinterp1 = Ipad(:, inds) .* x_weights + Ipad(:, inds_xshift) .* (1-x_weights); 
    J_xinterp2 = Ipad(:, inds_yshift) .* x_weights + Ipad(:, inds_xyshift) .* (1-x_weights);     

    J = J_xinterp1 .* y_weights + J_xinterp2 .* (1-y_weights);
end
