function [ wpts ] = translationWarp(pts, p)
    wpts = pts + repmat(p', size(pts, 1), 1);
end
