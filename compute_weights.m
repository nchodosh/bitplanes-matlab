function [ w ] = compute_weights(residuals)
% [ N, M ] = size(residuals);
% residuals = reshape(residuals, [ 8, N/8, M ]);
% residuals = reshape(sum(abs(residuals), 1), [ N/8, M ]);
% u = repmat(mean(residuals, 1), N/8, 1);
% b = repmat(mean(abs(residuals - u), 1), N/8, 1);
% W = mean(1/2 * exp(-abs(residuals - u) ./ b) ./ b, 2);
% w = repmat(W', 8, 1);
% w = w(:);
    [ N, ~ ] = size(residuals);
    u = repmat(mean(residuals, 1), N, 1);
    b = repmat(mean(abs(residuals - u), 1), N, 1);
    W = mean(1/2 * exp(-abs(residuals - u) ./ b) ./ b, 2);
    w = W(:);
    
end
