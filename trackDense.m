function [ new_pts, params, iters, residuals, imgs ] = trackDense(I1, I2, pts, params_init, ...
                                                      warpJacobian, ...
                                                      warp, compose_warp, ...
                                                      weights, get_debug, ...
                                                      iterations, min_disp)  


    num_pts = size(pts, 1);
    DOF = size(params_init, 1);
    d = 8;
    [nr, nc] = size(I1);
    M = zeros(nr, nc);
    M(sub2ind([nr, nc], pts(:, 2), pts(:, 1))) = 1;
    % phi{1,2} holds the bitplane for each pixel
    phi1 = compute_channels_nate(I1);
    phi1_perm = permute(phi1, [ 3 1 2 ] );

    
    %holds the derivatives of the bitplanes
    [ dpdx, dpdy ] = channel_derivs(phi1_perm);

    J = zeros(8*num_pts, DOF);
    for n = 1:num_pts
        wJ = warpJacobian(pts(n, :), zeros(DOF, 1));
        J(8*(n-1)+1 : 8*n, :) = [ dpdx(:, pts(n, 2), pts(n, 1)), ...
                            dpdy(:, pts(n, 2), pts(n, 1)) ] * wJ;
    end
    W = repmat(weights, 1, DOF);
    H = (W .* J)' * (W .* J);
    p = params_init;
    inds = sub2ind([nr, nc], pts(:, 2), pts(:, 1));
    
    [ x, y ] = meshgrid(1:nc, 1:nr);
    
    imgs = zeros(0);
    past_params = zeros(DOF, iterations);
    past_errors = zeros(iterations, 1);
    for m = 1:iterations
        past_params(:, m)  = p;
        warped_all_pts = warp([ x(:), y(:) ], p);
        wI2 = interpPts(reshape(I2, 1, nr, nc), warped_all_pts);
        wI2 = reshape(wI2, 1, nr, nc);
        wI2 = permute(wI2, [ 2 3 1 ] );
        wphi2 = compute_channels_nate(wI2);
        wphi2_perm = permute(wphi2, [ 3 1 2 ]);
        warped_target = wphi2_perm(:, inds);
        
        template = phi1_perm(:, inds);
        e = warped_target(:) - template(:);
        dp = H \ ((W .* J)' * (weights .* e));
        
        past_errors(m) = norm(e);
        
        %%debug
        if get_debug
            template = zeros(d, nr, nc);
            template(:, inds) = phi1_perm(:, inds);
            warped = zeros(d, nr, nc);
            warped(:, inds) = wphi2_perm(:, inds);
            
            template = permute(template, [ 2 3 1 ]);
            warped = permute(warped, [ 2 3 1 ]);
            scale = max([warped(:); template(:)]);
            bitimg = 255/scale * [ warped(:, :, 1), template(:, :, 1);
                                warped(:, :, 2), template(:, :, 2);
                                warped(:, :, 3), template(:, :, 3)
                                warped(:, :, 4), template(:, :, 4)
                                warped(:, :, 5), template(:, :, 5)
                                warped(:, :, 6), template(:, :, 6)
                                warped(:, :, 7), template(:, :, 7)
                                warped(:, :, 8), template(:, :, 8)];

            imgs(:, :, m) = [ M .* wI2, M .* I1; bitimg ];
            
            % warped_all_pts = warp([ x(:), y(:) ], p);
            % wI2 = interpPts(reshape(I2, 1, nr, nc), warped_all_pts);
            % wI2 = reshape(wI2, 1, nr, nc);
            % wI2 = permute(wI2, [ 2 3 1 ] );
            
            % inv_warped_all_pts = warp([ x(:), y(:) ], dp);
            % wI1 = interpPts(reshape(I1, 1, nr, nc), inv_warped_all_pts);
            % wI1 = reshape(wI1, 1, nr, nc);
            % wI1 = permute(wI1, [ 2 3 1 ] );
            
            %        figure(); imagesc( [ I1(:, :), wI1(:, :), wI2(:, :) ] ); axis image;
            %        colormap gray;
        end
        %%enddebug

        p = compose_warp(dp, p);

        %fprintf('delta: %f\n', norm(dp));
        %fprintf('Error: %f\n', norm(e));

        fprintf('deltaP: %f\n', norm(p - past_params(:,m)));
        if (norm(p - past_params(:, m)) < min_disp)
            break;
        end
    end
    iters = m;
    params = p;
    new_pts = warp(pts, p);
    residuals = e;
    if get_debug
        imgs = imgs(:, :, 1:m);
    end
end
