function [ pyr ] = makePtPyramid(pts, levels)
    pyr = cell(levels, 1);
    tmp = double(pts);
    for i = levels:-1:1
        cur_pts = round(tmp);
        %pyr{i} = unique(cur_pts, 'rows');
        pyr{i} = cur_pts;
        tmp = tmp/2;
    end
    
end