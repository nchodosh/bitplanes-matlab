
% snr = 11*10;
% snc = 11*10;


% I1 = [ zeros(2*snr/11, snc);
       
%        zeros(3*snr/11, 2*snc/11), 255*ones(3*snr/11, 3*snc/11), ...
%        zeros(3*snr/11, snc/11), 255*ones(3*snr/11, 3*snc/11), ...
%        zeros(3*snr/11, 2*snc/11);
       
%        zeros(snr/11, snc);
       
%        zeros(3*snr/11, 2*snc/11), 255*ones(3*snr/11, 3*snc/11), ...
%        zeros(3*snr/11, snc/11), 255*ones(3*snr/11, 3*snc/11), ...
%        zeros(3*snr/11, 2*  snc/11);
       
%        zeros(2*snr/11, snc) ];

% I1 = 255*imnoise(I1, 'localvar', 1*rand(snr, snc)/255);

% I1(I1 > 255) = 255;
% I1(I1 < 0) = 0;

% [ x, y ] = meshgrid(1:snc, 1:snr);
% tparams = [ -4; 2.2; 4.4; -10.2; 10.8; 13; -10.1; -13.1 ]; %+ repmat([1; -0.4], 4, 1);
% warped_pts = homographyWarp([ x(:), y(:) ], tparams, cpts);

% I2 = interpPts(reshape(I1, d, nr, nc), warped_pts);
% I2 = reshape(I2, d, nr, nc);
% I2 = permute(I2, [ 2 3 1 ] );



scale = 0.25;
I1 = imgaussfilt(double(imresize(rgb2gray(imread('violin1.png')), scale)), ...
                2);
I2 = imgaussfilt(double(imresize(rgb2gray(imread('violin2.png')), scale)), ...
                 2);

[ nr, nc, d ] = size(I1);
cpts = [ 0, 0;
         nc, 0;
         0, nr;
         nc, nr ];


% v = VideoReader('background_change.avi');

% frame1_full = readFrame(v);
% frame1 = imresize(frame1_full, scale);
% frame1_gray = double(imgaussfilt(rgb2gray(frame1), 2));

% [nr_full, nc_full, ~] = size(frame1_full);
% [nr, nc] = size(frame1_gray);

if (exist('old_nr', 'var') == 0 || exist('old_nc', 'var') == 0 || ...
    (old_nr ~= nr) || (old_nc ~= nc))
    h = figure();
    [ M, xi, yi ] = roipoly(uint8(I1));
    old_nr = nr;
    old_nc = nc;
    close(h);
end


inds = sub2ind([nr, nc], y(:), x(:));
inside = M(inds);
pts = [ x(inside), y(inside) ];


I1 = imgaussfilt(I1, 2);
I2 = imgaussfilt(I2, 2);




[ new_pts, params, iters, imgs ] = trackHomographyPyramid(I1, I2, pts, 2, zeros(8, 1), true, ...
                                                  500, 1, cpts);

num_pts = size(new_pts, 1);

figure(); imagesc([I1 I2]); axis image; colormap gray; hold on;
scatter(pts(1:30:num_pts, 1), pts(1:30:num_pts, 2));
scatter(new_pts(1:30:num_pts, 1) + size(I1, 2), new_pts(1:30:num_pts, 2));
