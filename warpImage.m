function [ J ] = warpImage(I, params, warp)
    [ nr, nc, d ] = size(I);
    I = permute(I, [ 3 1 2 ] );
    [ x, y ] = meshgrid(1:nc, 1:nr);
    warped_pts = warp([ x(:), y(:) ], params);
    J = interpPts(I, warped_pts);
    J = reshape(J, d, nr, nc);
    J = permute(J, [ 2 3 1 ]);
end