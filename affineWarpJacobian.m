function [ wJ ] = affineWarpJacobian(pt, ~)
    wJ = [ pt(1), pt(2), 0, 0, 1, 0;
           0, 0, pt(1), pt(2), 0, 1 ];
end
