function [ dpdx, dpdy ] = channel_derivs(phi)

[d, nr, nc] = size(phi);
[x, y] = meshgrid(1:nc, 1:nr);

dpdx = zeros(d, nr, nc);
dpdy = zeros(d, nr, nc);

xplusone = x + 1;
xplusone(xplusone > nc) = nc;
yplusone = y + 1;
yplusone(yplusone > nr) = nr;
xminusone = x - 1;
xminusone(xminusone < 1) = 1;
yminusone = y - 1;
yminusone(yminusone < 1) = 1;


xplusone_inds = sub2ind([nr, nc], y(:), xplusone(:));
yplusone_inds = sub2ind([nr, nc], yplusone(:), x(:));
xminusone_inds = sub2ind([nr, nc], y(:), xminusone(:));
yminusone_inds = sub2ind([nr, nc], yminusone(:), x(:));    

dpdx(:, :) = (phi(:, xplusone_inds(:)) - phi(:, xminusone_inds(:)))/2;
dpdy(:, :) = (phi(:, yplusone_inds(:)) - phi(:, yminusone_inds(:)))/2;

dpdx = reshape(dpdx, [ d nr nc ]);
dpdy = reshape(dpdy, [ d nr nc ]);


end