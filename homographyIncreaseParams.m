function [ new_params ] = homographyIncreaseParams(p, cpts)
    H = paramsToHomography(p, cpts);
    S = diag([2, 2, 1]);
    Si = diag([1/2, 1/2, 1]);    
    Hnew = S * H * Si;
    new_params = homographyToParams(Hnew, cpts);
end
