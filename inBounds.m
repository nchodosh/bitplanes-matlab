function [ inside ] = inBounds(pt, nr, nc, buffer)
inside = pt(1) >= 1 + buffer && pt(1) <= nc - buffer && ...
         pt(2) >= 1 + buffer && pt(2) <= nr - buffer;
end