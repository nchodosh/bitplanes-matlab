function [ wJ ] = translationWarpJacobian(~, ~)
    wJ = [ 1, 0;
           0, 1 ];
end