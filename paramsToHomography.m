function [ H ] = paramsToHomography( params, cpts )
    params = params + reshape(cpts', 8, 1);
    A = [ cpts(1, :), 1, 0, 0, 0, -params(1) * cpts(1, :), -params(1);
          0, 0, 0, cpts(1, :), 1, -params(2) * cpts(1, :), -params(2);
          cpts(2, :), 1, 0, 0, 0, -params(3) * cpts(2, :), -params(3);
          0, 0, 0, cpts(2, :), 1, -params(4) * cpts(2, :), -params(4);
          cpts(3, :), 1, 0, 0, 0, -params(5) * cpts(3, :), -params(5);
          0, 0, 0, cpts(3, :), 1, -params(6) * cpts(3, :), -params(6);
          cpts(4, :), 1, 0, 0, 0, -params(7) * cpts(4, :), -params(7);
          0, 0, 0, cpts(4, :), 1, -params(8) * cpts(4, :), -params(8) ];
    [ ~, ~, V ] = svd(A);
    h = V(:, end) / V(end, end);
    
    H = [ h(1), h(2), h(3);
          h(4), h(5), h(6);
          h(7), h(8), 1 ];
    
end
