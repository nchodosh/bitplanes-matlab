function [ I ] = makeCostImg(phi1, phi2, pt, param, rad)
    diam = 2*rad + 1;
    I = zeros(diam, diam, 3);
    max_err = diam*diam*8;
    [ nr, nc, ~ ] = size(phi1);
    for dx = -rad:rad
        for dy = -rad:rad
            if inBounds(pt' + round(param) + [ dx; dy ], nr, nc, rad);
                planes_roi = phi2((-rad:rad) + pt(2) + round(param(2)) + dy, ...
                                  (-rad:rad) + pt(1) + round(param(1)) + dx, ...
                                  :);
                warped_planes = imtranslateFrac(planes_roi, ...
                                                -(param - round(param))', ...
                                                'same');
                
                template_planes = phi1((-rad:rad) + pt(2), ...
                                       (-rad:rad) + pt(1), ...
                                       :);
            
                e = (warped_planes - template_planes).^2;
                I(dy+rad+1, dx+rad+1, :) = 255*sum(sum(sum(e)))/max_err;
            end
        end
    end
    
end
