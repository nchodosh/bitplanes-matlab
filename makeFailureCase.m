function [] = makeFailureCase(filename, imgs, frame0, frame1)
    filenames = { sprintf('failure_src/%s.mj2', filename), ...
                  sprintf('failure_src/%s_frame0.bmp', filename), ...
                  sprintf('failure_src/%s_frame1.bmp', filename), ...
                  sprintf('%s.zip', filename) };
    
    mkdir('failure_src');
    
    v = VideoWriter(filenames{1}, 'Archival');
    open(v);
    writeVideo(v, uint8(imgs));
    close(v);
    imwrite(uint8(frame0), filenames{2});
    imwrite(uint8(frame1), filenames{3});
    zip(filenames{4}, 'failure_src');
    rmdir('failure_src', 's');
end
