function [ J ] = makeTest( I, t, noise_level )
    J = imtranslateNate(I, t);
    noise = noise_level*rand(size(I));
    J = J + noise;
    J(J < 0) = 0;
    J(J > 255) = 255;
end