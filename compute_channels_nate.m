function [ phi ] = compute_channels_nate(I)
    [nr, nc] = size(I);
    phi = zeros(nr, nc, 8);
    [x, y] = meshgrid(1:nc, 1:nr);
    inds_0 = sub2ind([nr, nc], y(:), x(:));
    cnt = 1;
    
    lower = 0.95;
    upper = 1.05;
    %% Compute channels
    for dx = -1:1
        for dy = -1:1
            if dx ~= 0 || dy ~= 0
                xp = x + dx;
                yp = y + dy;
                xp(xp > nc) = nc;
                yp(yp > nr) = nr;
                xp(xp < 1) = 1;
                yp(yp < 1) = 1;
                inds = sub2ind([nr, nc], yp(:), xp(:));
                channels = double((lower*I(inds) < I(inds_0)) & (I(inds_0) < upper*I(inds)));
                channels(I(inds_0) < lower*I(inds)) = 0;
                channels(I(inds_0) > upper*I(inds)) = 2;
                phi(:, :, cnt) = reshape(channels, nr, nc);
                cnt = cnt + 1;
            end
        end
    end
end