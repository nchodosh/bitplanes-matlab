function [ h1, h2 ] = displayTrackedPoints(I1, I2, pts1, pts2, iters)
    
    n = size(pts1, 1);
    
    h1 = figure('Name', 'Points'); 
    subplot(1, 2, 1); imagesc(I1); axis image; colormap(gray);
    hold on; scatter(pts1(:, 1), pts1(:, 2)); hold off;
    subplot(1, 2, 2); imagesc(I2); axis image; colormap(gray); 
    hold on;  scatter(pts2(:, 1), pts2(:, 2)); hold off;
    
    h2 = figure('Name', 'Warped'); colormap(cool);
    for i = 1:n
        sub_I1 = I1((-10:10) + pts1(i, 2), (-10:10) + pts1(i, 1));
        sub_I2_warped = I2((-10:10) + round(pts2(i, 2)), ...
                           (-10:10) + round(pts2(i, 1)));
        sub_I2 = I2((-10:10) + pts1(i, 2), (-10:10) + pts1(i, 1));
        subplot(n, 3, 3*i - 2); imagesc(sub_I1);
        axis image;
        subplot(n, 3, 3*i - 1); imagesc(sub_I2_warped);
        axis image;
        title(sprintf('iter:%d, dx=%3.3f, dy=%3.3f', iters(i),...
                      pts2(i, 1) - pts1(i, 1),...
                      pts2(i, 2) - pts1(i, 2)));
        subplot(n, 3, 3*i + 0); imagesc(sub_I2);
        axis image; 
    end
end
