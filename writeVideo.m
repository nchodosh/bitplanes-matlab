function [] = writeVideo(video, filename)
 [ ~, ~, num_frames ] = size(video);
 v = VideoWriter(filename);
 open(v);
 for i = 1:num_frames
     writeVideo(v, video(:, :, i));
 end
 close(v);
end