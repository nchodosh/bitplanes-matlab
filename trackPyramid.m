function [ new_pts, params, iters, imgs ] = trackPyramid(I1, I2, pts, levels, ...
                                                      weights, varargin)

    num_pts = size(pts, 1);
    pyr1 = makePyramid(I1, levels);
    pyr2 = makePyramid(I2, levels);
    iters = zeros(num_pts , 1);
    ptPyr = makePtPyramid(pts, levels);

    if nargin < 6
        params_init = zeros(num_pts, 2);
        get_debug = false(num_pts, 1);        
    else
        params_init = varargin{1} / (2 ^ (levels - 1));
        if nargin < 7
            get_debug = false(num_pts, 1);
        else
            get_debug = varargin{2};            
        end
    end
    imgs = cell(num_pts, 1);
    [imgs{:}] = deal(zeros(0));
    for i = 1:levels
        [ new_pts, new_params, new_iters, new_imgs ] = track(pyr1{i}, pyr2{i}, ptPyr{i}, ...
                                                         weights, params_init, get_debug);
        params_init = new_params * 2;
        iters = iters + new_iters;
        for n = 1:num_pts
            if (numel(imgs{n}) == 0)
                imgs{n} = new_imgs{n};
            else
                imgs{n}(:, :, :, size(imgs{n}, 4)+1 : size(imgs{n}, 4)+size(new_imgs{n}, 4)) ...
                    = new_imgs{n};                
            end
        end
    end
    params = new_pts - pts;
end
