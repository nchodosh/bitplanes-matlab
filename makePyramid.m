function [ pyr ] = makePyramid(I, levels)
    pyr = cell(levels, 1);
    tmp = I;
    for i = levels:-1:1
        pyr{i} = tmp;
        tmp = impyramid(tmp, 'reduce');
    end
end