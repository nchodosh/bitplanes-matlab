
nr = 11*10;
nc = 11*10;


I1 = [ zeros(2*nr/11, nc);
       
       zeros(3*nr/11, 2*nc/11), 255*ones(3*nr/11, 3*nc/11), ...
       zeros(3*nr/11, nc/11), 255*ones(3*nr/11, 3*nc/11), ...
       zeros(3*nr/11, 2*nc/11);
       
       zeros(nr/11, nc);
       
       zeros(3*nr/11, 2*nc/11), 255*ones(3*nr/11, 3*nc/11), ...
       zeros(3*nr/11, nc/11), 255*ones(3*nr/11, 3*nc/11), ...
       zeros(3*nr/11, 2*  nc/11);
       
       zeros(2*nr/11, nc) ];

I1 = 255*imnoise(I1, 'localvar', rand(nr, nc)/255);
I1 = imgaussfilt(I1, 2);
I1(I1 > 255) = 255;
I1(I1 < 0) = 0;
%scale = 0.1;
%I1 = imgaussfilt(double(imresize(rgb2gray(imread('lighting0.png')), scale)), ...
%                 2);
[ nr, nc, d ] = size(I1);

h = figure();
[ M, xi, yi ] = roipoly(uint8(I1));
close(h);
[ x, y ] = meshgrid(1:nc, 1:nr);
inds = sub2ind([nr, nc], y(:), x(:));
inside = M(inds);
pts = [ x(inside), y(inside) ];


x_ext = 0.1;
ext = 1.08;
m = 0.0;
sx = 1;
sy = 1;
angle = 0;
A = [cos(angle), sin(angle); -sin(angle), cos(angle)] * [ 1 m; 0 1] * ...
    [ sx 0; 0 sy ];
D = [ 0; 0];

params_init = [ A(1, 1) - 1; A(1, 2); A(2, 1); A(2,2) - 1; D(1); D(2) ];

Dinv = -A \ D;
Ainv = inv(A);

warped_pts = affineWarp([ x(:), y(:) ], ...
                        [ Ainv(1,1) - 1; Ainv(1,2); Ainv(2, 1); Ainv(2, 2)- 1; Dinv(1); Dinv(2) ]);


I2 = interpPts(reshape(I1, d, nr, nc), warped_pts);
I2 = reshape(I2, d, nr, nc);
I2 = permute(I2, [ 2 3 1 ] );

%[ pt_x, pt_y ] = meshgrid(1:nc, 1:nr);
%pts = [ pt_x(:), pt_y(:) ];


[ new_pts, params, iters, imgs ] = trackAffine(I1, I2, pts, zeros(6, 1), true, ...
                                               500);
num_pts = size(new_pts, 1);

figure(); imagesc([I1 I2]); axis image; colormap gray; hold on;
scatter(pts(1:30:num_pts, 1), pts(1:30:num_pts, 2));
scatter(new_pts(1:30:num_pts, 1) + size(I1, 2), new_pts(1:30:num_pts, 2));
                    
                    