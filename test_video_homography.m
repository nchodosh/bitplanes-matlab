levels = 1;
scale = 0.2;
start_frame = 1;


output_file = 'violin-stable-part-2bit.avi';
vw = VideoWriter(output_file);
open(vw);

video_file = 'GOPR0005.MP4';
v = VideoReader(video_file);
% if exist('old_video_file', 'var') == 0 || ...
%         strcmp(old_video_file, video_file) == 0
%     disp('Reading video...');
%     num_frames = 1;    
%     frames = cell(0);    
%     last_msg_len = 0;    
%     while hasFrame(v)
%         fprintf(repmat('\b',1,last_msg_len));
%         msg = sprintf('Reading Frame %d', num_frames);
%         fprintf(msg);
%         last_msg_len = length(msg);
%         frames{num_frames} = readFrame(v);
%         num_frames = num_frames + 1;
%     end
%     old_video_file = video_file;
% end

for i = 1:(start_frame-1)
    readFrame(v);
end

frame1_full = readFrame(v);
frame1 = imresize(frame1_full, scale);
frame1_gray = imgaussfilt(double(imresize(rgb2gray(frame1_full), scale)), 2);
frame2 = frame1;
[nr_full, nc_full, ~] = size(frame1_full);
[nr, nc] = size(frame1_gray);
cpts = [ 0, 0;
         nc, 0;
         0, nr;
         nc, nr ];


%sample_width = 5;
%figure('Name', 'Select Hand'); imagesc(frame1); bbox  = getrect; close;
%bbox = round(bbox);
% [x, y] = meshgrid(bbox(1):sample_width:(bbox(1) + bbox(3)), ...
%                  bbox(2):sample_width:(bbox(2) + bbox(4)));
% pts = [ x(:), y(:) ];
% num_pts = size(pts, 1);
%points = detectMinEigenFeatures(frame1_gray, 'ROI', bbox);
%pts = round(points.selectStrongest(num_pts).Location);

if (exist('old_nr', 'var') == 0 || exist('old_nc', 'var') == 0 || ...
    exist('old_start_frame', 'var') == 0 || ...
    (old_nr ~= nr) || (old_nc ~= nc) || (old_start_frame ~= start_frame))
    
    h = figure();
    [ M, xi, yi ] = roipoly(frame1);
    old_nr = nr;
    old_nc = nc;
    old_start_frame = start_frame;
    close(h);
    
    [ x, y ] = meshgrid(1:nc, 1:nr);
    inds = sub2ind([nr, nc], y(:), x(:));
    inside = M(inds);
    pts = [ x(inside), y(inside) ];
end

num_pts = size(pts, 1);
new_pts = pts;
frame_num = start_frame + 1;



imgs = cell(0);
traj = {pts/scale};
iters = cell(0);

cnt = 1;
get_debug = true;
residuals = zeros(0);

w = ones(8*num_pts, 1);

cur_params = zeros(8, 1);

weights = {w};
params = {cur_params};

writeVideo(vw, uint8([ frame1_gray, double(M) .* frame1_gray ]));

while frame_num < 50
    fprintf('Processing frame %d\n', frame_num);
    

    frame2_full = readFrame(v);
    frame2 = imresize(frame2_full, scale);
    frame2_gray = imgaussfilt(double(imresize(rgb2gray(frame2_full), scale)), 2);

    [ new_pts, new_params, new_iters, fresiduals, new_imgs ] = ...
        trackHomographyPyramid(frame1_gray, ...
                               frame2_gray, pts,...
                               levels, cur_params, true, ...
                               100, 0.9, w, cpts);
    
    residuals = cat(2, residuals, fresiduals);
    w = compute_weights(residuals(:, max([1 size(residuals, 2)-5]):end));
    
    imgs{cnt} = new_imgs;
    iters{cnt} = new_iters;

    cnt = cnt + 1; 
    
    weights{cnt} = w;   
    params{cnt} = cur_params;
    traj{cnt} = new_pts/scale;

    
    cur_params = new_params;
    frame_num = frame_num + 1;    
    
    output_frame = warpImage(frame2_gray, cur_params, ...
                             @(pts, p)homographyWarp(pts, p, cpts));
    
    % output_mask = warpImage(double(M), cur_params, ...
    %                         @(pts, p)homographyWarp(pts, p, cpts));
    output_mask = double(M) .* output_frame;

    writeVideo(vw, uint8([ output_frame, output_mask ]));
end

close(vw);



