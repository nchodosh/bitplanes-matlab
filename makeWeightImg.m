function [ I ] = makeWeightImg(w, pts, nr, nc)

    I = zeros(8, nr, nc);
    pt_inds = sub2ind([nr, nc], pts(:, 2), pts(:, 1));

    for i = 1:8
        I(i, pt_inds) = reshape(w(i:8:end), 1, size(pt_inds, 1));
    end

    I = makeMultiPlaneImg(I);
end



