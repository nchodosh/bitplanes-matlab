function [ pts ] = getPointsInPoly(I, M, bbox, n)
    se = strel('disk', 5);
    M_filt = imgaussfilt(double(imdilate(M, se)), 4);
    I_roi = uint8(double(I) .* M_filt);
    I_roi = I_roi(bbox(2):bbox(4), bbox(1):bbox(3));
    points = detectMinEigenFeatures(I_roi);
    pts = round(points.selectStrongest(2*n).Location);
    pts = pts + repmat([bbox(1), bbox(2)], size(pts, 1), 1);
    [ nr, nc ] = size(I);
    inds = sub2ind([nr nc], pts(:, 2), pts(:, 1));
    inside = M(inds);
    pts = pts(inside, :);
    pts = pts(1:min([n, size(pts, 1)]), :);
end
