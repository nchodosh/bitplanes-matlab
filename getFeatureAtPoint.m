function [ idx ] = getFeatureAtPoint(I, pts)
    h = figure('Name', 'Select Feature'); imagesc(uint8(I)); axis image;
    [x, y] = ginput(1); close(h);
    n = size(pts, 1);
    dists = sum((pts - repmat([x y], n, 1)).^2, 2);
    [ ~, idx ] = min(dists);
end