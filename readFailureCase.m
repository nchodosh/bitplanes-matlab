function [ video, I1, I2 ] = readFailureCase(filename)
    filenames = { sprintf('failure_src/%s.mj2', filename), ...
                  sprintf('failure_src/%s_frame0.bmp', filename), ...
                  sprintf('failure_src/%s_frame1.bmp', filename), ...
                  sprintf('%s.zip', filename) };
    
    unzip(filenames{4});
    v = VideoReader(filenames{1});
    video = readFrame(v);
    frame_num = 2;
    while hasFrame(v)
        video(:, :, :, frame_num) = readFrame(v);
        frame_num = frame_num + 1;
    end
    I1 = imread(filenames{2});
    I2 = imread(filenames{3});    
    rmdir('failure_src', 's');
    
    figure('Name', 'Failure Frames'); imagesc([ I1, I2 ]); axis image;
    implay(video);
end