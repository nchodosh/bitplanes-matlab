function [ new_params ] = homographyReduceParams(p, cpts)
    H = paramsToHomography(p, cpts);
    S = diag([2, 2, 1]);
    Si = diag([1/2, 1/2, 1]);    
    Hnew = Si * H * S;
    new_params = homographyToParams(Hnew, cpts);
end
