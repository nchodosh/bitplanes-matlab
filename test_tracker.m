function [ params, imgs ] = test_tracker(test)

close all;
switch test
  case 'lighting'
    
    fprintf(['Testing sudden lighting change. Most points undergo a translation \n' ...
             'of ~(-3.2, -4). I then shifted the image further by [ -4, -1] \n' ...
             'for a final translation of ~(-7.2, -5)\n']);

    I1 = double(imresize(rgb2gray(imread('lighting0.png')), 0.40));
    I2 = double(imresize(rgb2gray(imread('lighting1.png')), 0.40));
    I2 = makeTest(I2, [0, 0], 0);
    I1 = imgaussfilt(I1, 2);
    I2 = imgaussfilt(I2, 2);
    num_levels = 3;
    
    [ pts ] =  getBestPoints(I1, num_levels);
  case 'translate'
    fprintf(['Testing artificial translation. The image was shifted \n' ...
             'by ~(2, -1) and has some noise added to it.\n' ]);

    I1 = double(imresize(rgb2gray(imread('frame0.png')), 0.40));
    I2 = makeTest(I1, [-12.4, 4.1], 1);
    I1 = imgaussfilt(I1, 2);
    I2 = imgaussfilt(I2, 2);
    
    num_levels = 3;

    [ pts ] =  getBestPoints(I1, num_levels);
  case 'rotate'
    fprintf('Testing in plane rotation. The image was rotated by 10 degress \n');
    I1 = double(imresize(rgb2gray(imread('frame0.png')), 0.40));
    I2 = imrotate(I1, 10, 'crop');
    I1 = imgaussfilt(I1, 2);
    I2 = imgaussfilt(I2, 2);
    
    num_levels = 5;
    
    [ pts ] =  getBestPoints(I1, num_levels);
  case 'single'
    fprintf('Testing a single translated point\n');
  otherwise
    disp('ERROR: unrecognized test');
end

weights = ones(21, 21, 8);


[ new_pts, params, iters, imgs ] = trackPyramid(I1, I2, pts, num_levels,...
                                                weights, zeros(5, 2), ...
                                                [ 1 1 0 0 0]);
params %#ok
displayTrackedPoints(I1, I2, pts, new_pts, iters);

end

function [ pts ] = getBestPoints(I1, num_levels)

[nr, nc] = size(I1);

num_pts = 5; % number of points to track

border = 5*2^num_levels;
points = detectMinEigenFeatures(I1((border+1):(nr-border), (border+1):(nc-border))); %% Should I use the SIFT detector here?
pts = round(points.selectStrongest(num_pts).Location) + ...
      repmat([ border, border ], [ num_pts, 1]);
end